FROM registry.gitlab.com/src_prepare/gentoo-repoman-container:latest

ARG EMERGE_DEFAULT_OPTS=""
ARG FEATURES=""
ARG PORTAGE_BINHOST=""

RUN mkdir -p /etc/portage/{repos.conf,package.accept_keywords} \
&& echo "*/*::src_prepare-overlay **" > /etc/portage/package.accept_keywords/src_prepare-overlay.conf \
&& echo -e "[src_prepare-overlay]\nauto-sync = yes\nlocation = /var/db/repos/src_prepare-overlay\nsync-type = git\nsync-uri = https://gitlab.com/src_prepare/src_prepare-overlay.git" > /etc/portage/repos.conf/src_prepare-overlay.conf \
&& emaint sync -r src_prepare-overlay \
&& emerge app-eselect/eselect-repository app-portage/euscan-ng sys-apps/busybox
